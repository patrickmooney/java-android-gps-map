package com.casualheroes.gps;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Marker;
import android.R.drawable;

import android.app.Activity;
import android.support.v4.app.NotificationCompat.Builder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import java.util.HashMap;
import java.lang.String;
import android.text.Html;
import android.widget.TextView; 
import android.support.v4.app.TaskStackBuilder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import java.util.ArrayList;


public class GPS extends FragmentActivity {
    /** Called when the activity is first created. */
    
     private LocationManager lm;
     private LocationListener locationListener;
     private double mLongitude = 0;
     private double mLatitude = 0;
     private GoogleMap supportMap = null;
     
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.main );
        setUpMapIfNeeded();     
    }

public void notification ( String title, String description ) {
Builder mBuilder =
        new Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle(title)
        .setContentText(description);

// Creates an explicit intent for an Activity in your app
Intent resultIntent = new Intent(this, GPS.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
stackBuilder.addParentStack(GPS.class);
// Adds the Intent that starts the Activity to the top of the stack
stackBuilder.addNextIntent(resultIntent);
PendingIntent resultPendingIntent =
        stackBuilder.getPendingIntent(
            0,
            PendingIntent.FLAG_UPDATE_CURRENT
        );
mBuilder.setContentIntent(resultPendingIntent);
NotificationManager mNotificationManager =
    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.

Notification note = mBuilder.build();
note.defaults |= Notification.DEFAULT_SOUND;
note.defaults |= Notification.DEFAULT_VIBRATE;

mNotificationManager.notify(105, note);
        }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (supportMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            supportMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (supportMap != null) {
                supportMap.setOnInfoWindowClickListener( new OnInfoWindowClickListener() {
                    public void onInfoWindowClick(Marker marker) {
                        markerClick(marker);
                    }
                });
                supportMap.setMyLocationEnabled(true);
                setUpMap();
            }
        }
    }

    private void setUpMap () {
         setStatus( "Map is set up! - getting lat long" );
         lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE); 
            locationListener = new MyLocationListener();
            lm.requestLocationUpdates(
            LocationManager.GPS_PROVIDER, 
            1000 * 10,       // ms since last call
            0,         // 10 m movement
            locationListener);

        lm.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    1000 * 10,
                    0,
                    locationListener
                    );
    }   

    public void setStatus ( String status ){
        TextView tv = (TextView) findViewById(R.id.textbox);
        tv.setText( status );
        notification ( "Casual Hero Update", status );
    }

    public void markerClick ( Marker marker ){
        setStatus( "MARKER CLICKED - " + marker.getTitle() );
        Pattern p = Pattern.compile("^([0-9]+):");
        Matcher m = p.matcher(marker.getTitle());

        if (m.find()) {
            setStatus("ID: " + m.group(1));
            marker.setTitle("[PARTICIPATING]" + marker.getTitle() );

            GetAndPost gnp = new GetAndPost();
            HashMap<String,String> fvp = new HashMap<String,String>();
            fvp.put( "RequestId", m.group(1) );
            fvp.put( "UserId", "11" );

            String resp = gnp.postData( "http://casual-heroes.org/AcceptedRequests/Create", fvp );
            setStatus( resp );
        }
        else {
            setStatus("Already taking part");
        }
    }

    public class MyLocationListener implements LocationListener 
    {
        public void onLocationChanged(Location loc) {           
             
             if (loc != null) {


                 mLongitude = loc.getLongitude();
                 mLatitude = loc.getLatitude();

                 //HashMap<String,String> fvp = new HashMap<String,String>();
                 //fvp.put( "Latitude", String.valueOf( mLatitude ) );
                 //fvp.put( "Longitude", String.valueOf( mLongitude ) );
                 setStatus( "GOT LAT LONG - " + String.valueOf( mLatitude ) + "x" + String.valueOf( mLongitude ) );


                 GetAndPost gnp = new GetAndPost();
                 String response = gnp.getData( "http://casualheroes.azurewebsites.net/Requests?Latitude=" + String.valueOf( mLatitude )
                                               + "&Longitude=" + String.valueOf( mLongitude ) );


                 setStatus( "GOT SERVER RESP" );
                 Gson gson = new Gson();
                 MarkerArray markerArray = null;
                 try {
                     markerArray = gson.fromJson( response, MarkerArray.class );
                 } catch ( Exception e ){
                     setStatus( "BAD JSON - " + e.getMessage() );return;
                 }


                 //TextView tv = (TextView) findViewById(R.id.textbox);
                 //tv.setText( response );

                 setStatus( "FOUND: " + markerArray.Data.size() + " points" );
                 //setStatus( response );
                 ArrayList<MarkerOptions> markerOptions = new ArrayList<MarkerOptions>();
                
                 for ( int m_it = 0; m_it < markerArray.Data.size(); m_it++ ){
                    CharityMarker m = markerArray.Data.get(m_it);
                    String title = m.RequestId + ":" + m.Title;
                    for ( CharityUser user : m.Participants ) {
                        if ( user.UserId == 11 )
                            title = "[PARTICIPATING]"+title;
                    }
                    markerOptions.add( new MarkerOptions().position( new LatLng( m.Latitude, m.Longitude ) ).title(title) );
                 }
                 for ( int m_it = 0; m_it < markerOptions.size(); m_it++ ){
                     supportMap.addMarker( markerOptions.get( m_it ) );
                 }

//                 MarkerOptions marker = new MarkerOptions();
//                 marker.position( new LatLng( mLatitude, mLongitude ) ); 
//                 supportMap.addMarker( marker );
                 //String s1 = String.format( "http://maps.google.ca/maps?ll=%f,%f",mLatitude,mLongitude);

//                 GetAndPost gPost = new GetAndPost();
//                 String resp = gPost.postData( "http://casualheroes.azurewebsites.net/", fvp );  
//                TextView textView = (TextView) findViewById(R.id.edit_message);
//                textView.setText( resp, TextView.BufferType.EDITABLE );
                 //Log.e("Hello7",s1); 
            }// loc not null
        }

        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
            Log.d("Hello7","onProviderEnabled");
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
          }


        
}
    // JSON CLASSES
    private class MarkerArray { ArrayList<CharityMarker> Data; }
    private class CharityMarker {
        int RequestId;
        Double Latitude, Longitude;
        String Title, Description, Address, CreatedBy, CreatedOn, StartDate, EndDate;
        ArrayList<String> Tags;
        ArrayList<CharityUser> Participants;
    }
    private class CharityUser { 
        int UserId;
        String Identifier, FirstName, LastName, Email, PhoneNumber, DetailsUrl;
    }
}
