package com.casualheroes.gps;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;

public class GetAndPost {

    public String getData ( String URL ){
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet( URL );

        String resp_str = null;

        try {
            HttpResponse response = httpclient.execute(httpget);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;
            
            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }  

            /* Convert the Bytes read to a String. */
            resp_str = new String(baf.toByteArray());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return resp_str;
    }

    public String postData( String URL, HashMap<String,String> fvp ){  
        // Create a new HttpClient and Post Header

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost( URL );  

        String resp_str = null;

        try {

            Iterator it = fvp.entrySet().iterator();
            List nameValuePairs = new ArrayList();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry)it.next();
                BasicNameValuePair nvp = new BasicNameValuePair( (String)pairs.getKey(), (String)pairs.getValue() );
                nameValuePairs.add(nvp);
                it.remove(); // avoids a ConcurrentModificationException
            }

            // Add your data
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));  

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;
            
            while((current = bis.read()) != -1){
                baf.append((byte)current);
            }  

            /* Convert the Bytes read to a String. */
            resp_str = new String(baf.toByteArray());
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return resp_str;
    }
}
